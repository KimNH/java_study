package calendar;

import java.util.Calendar;

public class CalendarExam1 {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();

		// 년
		int year = cal.get(Calendar.YEAR);
		System.out.println("현재 년도 : " + year);

		// 월 (0 ~ 11)
		int month = cal.get(Calendar.MONTH);
		System.out.println("현재 월 : " + month);
		// 해당 월의 일자 (1 ~ 해당 월의 마지막 날)
		int date = cal.get(Calendar.DAY_OF_MONTH);
		int date2 = cal.get(Calendar.DATE); // DAY_OF_MONTH와 동일
		System.out.println("현재 일 : " + date + ", " + date2);
		// 요일 (일:1 ~ 토:7)
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		System.out.println("요일 : " + dayOfWeek);

		// 현재 일이 해당 월의 몇번째 주인지 출력
		int dayOfWeekInMonth = cal.get(Calendar.DAY_OF_WEEK_IN_MONTH);
		System.out.println("현재 월의 몇번째 주 : " + dayOfWeekInMonth);

		// 1년 중 몇번째 날짜인지 출력
		int dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
		System.out.println("현재 년도의 몇번째 일 : " + dayOfYear);
		// 시, 분, 초 출력
		int hour = cal.get(Calendar.HOUR_OF_DAY); // 0 ~ 23
		int hour2 = cal.get(Calendar.HOUR); // 0 ~ 11
		int ampm = cal.get(Calendar.AM_PM); // 오전:0, 오후:1
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		System.out.println(hour + "시 " + hour2 + "시 " + ampm + "(오전오후) " + min + "분" + sec + "초");
		int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		System.out.println("현재 월의 마지막 일 : " + lastDay);
	}

}




