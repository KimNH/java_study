package calendar;

import java.util.Calendar;

public class CalendarExam2 {
	public static void main(String[] args) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		// 2007년 3월 27일로 날짜 지정
		cal1.set(Calendar.YEAR, 2007);
		cal1.set(Calendar.MONTH, 2);
		cal1.set(Calendar.DATE, 27);
		cal1.set(2007, 2, 27); // 위 3줄과 동일한 기능

		// 날짜 차이 구하기
		long diffSec = cal2.getTimeInMillis() - cal1.getTimeInMillis();
		diffSec = diffSec / 1000; // 밀리초 -> 초
		System.out.println(diffSec + "초");

		int diffHour = (int) (diffSec / (60 * 60));
		System.out.println(diffHour + "시간");

		int diffDay = (int) (diffSec / (60 * 60 * 24));
		System.out.println(diffDay + "일");
	}

}




