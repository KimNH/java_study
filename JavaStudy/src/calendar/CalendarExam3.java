package calendar;

import java.util.Calendar;

public class CalendarExam3 {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1; // 0 ~ 11월
		int day = cal.get(Calendar.DAY_OF_MONTH);

		System.out.println(year + "" + month + "" + day);

		String source = "20070327";
		String test = source.substring(0, 4); // 2007
		
		int test2 = Integer.parseInt(test) + 1;
		float test3 = Float.parseFloat(test);
		Double test4 = Double.parseDouble(test);

		cal.set(Calendar.YEAR, 1994);
		cal.set(Calendar.MONTH, 2); // 4월로 지정
		cal.set(Calendar.DAY_OF_MONTH, 1); // 1일로 지정

		cal.add(Calendar.DAY_OF_MONTH, -1); // -1일 => 3월의 마지막 날짜로 이동
		// 날짜 출력
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1; // 0 ~ 11월
		day = cal.get(Calendar.DAY_OF_MONTH);

		
		String m = "";
		if(month < 10) {
			m = "0" + month;
		} else {
			m = "" + month;
		}
		
		String fullDate = "" + year + (month < 10 ? "0" + month : month) + (day < 10 ? "0" + day : day);
		
		
		System.out.println(fullDate);

	}

}










