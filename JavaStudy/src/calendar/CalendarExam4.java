package calendar;

import java.util.Calendar;

public class CalendarExam4 {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();

		// 현재 월의 1일로 설정
		cal.set(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, 1);

		// 7
		int startDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

		System.out.println(" SU MO TU WE TH FR SA");
		for (int i = 1; i < startDayOfWeek; i++) {
			System.out.print("   ");
		}
		
		// 30
		int endDate = cal.getActualMaximum(Calendar.DATE);
		
		for (int i = 1, n = startDayOfWeek; i <= endDate; i++, n++) {
			System.out.print((i < 10 ? "  " + i : " " + i));
			if (n % 7 == 0) {
				System.out.println();
			}
		}
	}

}
