package ch02;

import java.awt.Color;

public class Casting {
	public static void main(String[] args) {
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println(Color.RED);
		
		System.out.println((int) ('A')); // 65
		System.out.println((short) ('a')); // 97
		System.out.println((char) (95)); // _
		System.out.println((int) (1.414)); // 1
		System.out.println((float) (10)); // 10.0
		long num1 = 100L;
		int num2 = (int) num1; // 강제 형변환
		System.out.println(num2);

		int num3 = 100;
		float num4 = 2.2f;
		
		num3 = (int)num4;
		// 자동 형변환
		System.out.println((num3 * num4));

		// 의도적읶 표시
		double sum = 15.1 + (double) 10;
		System.out.println(sum);
		
		System.out.println(  (int)(2.2 * 1.1)    );
		
	}

	
	
	
	
	
	
	
	
}

