package ch02;

public class Overflow {
	public static void main(String[] args) {
		byte b = 127;
		b = (byte) (b + 1); // -128
		System.out.println(b);
		
		int max = Integer.MAX_VALUE;
		float fMax = Float.MAX_VALUE;
		System.out.println(max);
		System.out.println(fMax);
		System.out.println();

		int num1 = 256;
		byte num2 = (byte) (num1); // 44
		System.out.println(num2);
		
		int a = +5;
		int b2 = 0;
		a++;
		b2 = a;
		System.out.println(b2);
	}
}















