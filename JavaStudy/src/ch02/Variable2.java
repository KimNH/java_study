package ch02;

import java.awt.Color;

public class Variable2 {
	public static void main(String[] args) {
		// 논리
		boolean isFile = false; // true

		// 문자
		char ch = 'A';
		String str = "HI";

		// 숫자 (정수)
		byte bt = 10;
		short count = 100;
		int age = 2000000000;
		long ms = 3000000000l;

		// 숫자 (실수)
		float avg = 99.99f;
		double pro = 1234.12312313;

		// 허용 특수문자
		int abc$;
		int $abc;
		int abc_;
		int _abc;
		// 사용불가
		// int abc@;
		// int !abc;
		// int 123b;

	}
}
