package ch04;

public class ForExam1 {
	public static void main(String[] args) {
		int sum = 0;
		// Ctrl + Z : Undo (되돌리기)
		// Ctrl + Y : Redo (되돌리기 취소)
		for (int i = 1; i <= 10; i += 2) {
			sum += i; // sum = sum + i;
		}
		System.out.println(sum);
	}

}
