package ch04;

import java.util.Scanner;

public class Star1 {
	public static void main(String[] args) {
		// 사용자에게 값을 입력받는 라이브러리 Scanner
		Scanner s = new Scanner(System.in); // 생성 (메모리에 생성)
		System.out.println("값을 입력해주세요... ");
		int num = s.nextInt(); // 사용자가 값을 입력한 후 엔터를 입력할때까지 대기
		
		for (int a = 1; a <= num; a++) {
			for (int b = 1; b <= num; b++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		for(int a = 5; a >= 1; a--) {
			for(int b = 1; b <= a; b++) {
				System.out.print("@");
			}
			System.out.println();
		}
	}
}
