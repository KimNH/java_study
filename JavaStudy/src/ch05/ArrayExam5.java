package ch05;

public class ArrayExam5 {
	public static void main(String[] args) {
		int[][] arr1 = new int[][] { 
			{ 1, 2, 3 }, 
			{ 4, 5, 6 }, 
			{ 7, 8 }, 
			{ 10 } 
		};
		for(int i = 0; i < arr1.length; i++) {
			int[] arr2 = arr1[i];
			for(int j = 0; j < arr2.length; j++) {
				System.out.println(arr2[j]);
			}
		}
		for(int i = 0; i < arr1.length; i++) {
			for(int j = 0; j < arr1[i].length; j++) {
				System.out.println(arr1[i][j]);
			}
		}
	}

}







