package ch06;

public class Factorial1 {
	public static void main(String[] args) {
		int number = 30;
		int result = factorial(number);
		System.out.println(result);
	}

	public static int factorial(int n) {
		if (n == 1) {
			return 1;
		} else {
			return n * factorial(n - 1);
		}
	}

}
