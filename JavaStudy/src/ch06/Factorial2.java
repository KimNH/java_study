package ch06;

public class Factorial2 {
	int name;
	int age;
	
	public static void main(String[] args) {
		int number = 5;
		int result = factorial(number, 1);
		System.out.println(result);
	}

	public static int factorial(int n, int result) {
		if (n == 1) {
			return result;
		} else {
			return factorial(n - 1, result * n);
		}
	}

}
