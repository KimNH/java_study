package ch06;

public class MethodExam2 {
	// Ctrl + Shift + F : 소스 정렬
	public static void main(String[] args) {
		String result = printStar(5, '★');
		System.out.println(result);
		// 메소드 호출
		int sum = getTotal(100);
		float avg = sum / 100f;
		System.out.println(avg);
	}
	
	public static int getTotal(int num) {
		int total = 0;
		for (int i = 1; i <= num; i++) {
			total = total + i;
		}
//		System.out.println(total);
		return total;
	}

	public static String printStar(int count, char ch) {
		String star = "";
		for (int i = 1; i <= count; i++) {
			for (int j = 1; j <= i; j++) {
				star = star + ch;
			}
			star = star + "\n";
		}
		return star;
	}

}
