package ch06;

import java.util.Random;

public class MethodExam3 {
	public static void main(String[] args) {
		int number = getRandomNumber(60, 100);
		System.out.println(number);
	}

	public static int getRandomNumber(int startNum, int endNum) {
		// 1
		int num = (int)(Math.random() * 3) + 1; // 0.0 ~ 2.999999999999
//		int num = random.nextInt(3) + 1;
		//  0.0 <= x < 1.0
		System.out.println(0.9999999999999 * 45);
		System.out.println(0.9999999999999 * 0);
		
		// 2
		Random random = new Random();
		int number = 0;
		while (true) {
			number = random.nextInt(endNum);
			if (number >= startNum) {
				break;
			}
		}
		return number;
	}
}




