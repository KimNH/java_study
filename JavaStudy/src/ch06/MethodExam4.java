package ch06;

public class MethodExam4 {
	static int num = 10;

	public static void main(String[] args) {
		int num = 20;
		System.out.println(num);
	}

	public static void temp() {
		int num = 30;
		System.out.println(num);
	}

	// 지역변수가 없는경우 전역변수 참조
	public static void temp2() {
//		int num = 30;
		System.out.println(num);
	}
	
//	 지역변수가 동시에 존재하는 경우 전역변수 참조법
	public void temp3() {
		int num = 30;
		System.out.println(this.num);
		System.out.println(num);
	}
}






