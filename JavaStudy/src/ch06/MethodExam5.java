package ch06;

public class MethodExam5 {
	public static void method1(int num) { // 기본자료형
		num = num * 2;
	}
	public static void method2(int[] nums) { // 참조자료형
		nums[0] = nums[0] * 2;
	}
	
	public static void main(String[] args) {
		int n = 10;
		int[] ns = {10};
		
		method1(n);
		method2(ns);
		
		System.out.println("n : " + n);
		System.out.println("ns : " + ns[0]);
		
		
		
		
		int num1 = 10;
		int num2 = 10;
		System.out.println(num1 == num2);

		int[] nums1 = { 1, 2 };
		int[] nums2 = { 1, 2 };
		System.out.println(nums1 == nums2);
		System.out.println(nums1.hashCode());
		System.out.println(nums2.hashCode());

		nums1 = nums2;
		System.out.println(nums1 == nums2);
		System.out.println(nums1.hashCode());
		System.out.println(nums2.hashCode());
	}
}
