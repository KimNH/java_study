package ch07;

public class SmartPhone {
	String company; // 제조사
	String name; // 단말기명
	float size; // 핸드폰 사이즈 ex)4.0, 5.0, 5.3
	int price; // 가격

	public void volumeUp() {
		System.out.println(name + " 소리 올림");
	}

	public void volumeDown() {
		System.out.println(name + " 소리 내림");
	}

	public void powerOn() {
		System.out.println(name + " 젂원 켜짐");
	}

	public void powerOff() {
		System.out.println(name + " 젂원 꺼짐");
	}
}
