package ch07;

public class Static {
	static char color = 'R';
	static int num = 0;
	String name;

	static void staticMethod() {
		System.out.println("static method");
		num++;
	}

	void instanceMethod() {
		String name = null;
//		this.name = name;
		System.out.println(name);
		System.out.println("instance method");
	}

}
