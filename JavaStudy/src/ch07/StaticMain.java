package ch07;

import java.awt.Color;

import javax.swing.JFrame;

public class StaticMain {
	public static void main(String[] args) {
		JFrame j = new JFrame();
		j.setBackground(new Color(255, 0, 0));
		j.setBackground(Color.RED);
		j.setSize(200, 100);
		j.setVisible(true);
		
		// 클래스명을 이용하여 접근
		System.out.println(Static.color);
		Static.staticMethod();
		System.out.println(Static.num);
		// 객체를 생성시킨 후 접근
		Static s = new Static();
		s.instanceMethod();
		s.num++;
		// 아래처럼 사용은 가능하지만 추천하지 않음
		System.out.println(s.num);
	}
}
