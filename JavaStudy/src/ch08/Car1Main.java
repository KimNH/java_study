package ch08;

public class Car1Main {
	public static void main(String[] args) {
		Car1 car = new Car1();
		car.color = "red";
		
		Car1 car2 = new Car1();
		car2.color = "blue";
		
		Car1[] cars = {car, car2};
		
		Car1[] cars2 = {new Car1("red"), new Car1("blue")};
		
		
		System.out.println(car.color);
		System.out.println(car.door);
	}
}
