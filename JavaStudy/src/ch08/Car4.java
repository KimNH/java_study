package ch08;

public class Car4 {
	String color;
	int door;
	int gearType; // 0 : 수동, 1 : 자동
	
	

	public String getColor() {
		this.setColor("ddd");
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getDoor() {
		return door;
	}

	public void setDoor(int door) {
		this.door = door;
	}

	public int getGearType() {
		return gearType;
	}

	public void setGearType(int gearType) {
		this.gearType = gearType;
	}

	public Car4() {
		this("파랑", 4); // 자신의 생성자 호출
	}

	public Car4(String color) {
		this(color, 4);
	}

	public Car4(int d, String c) {
		color = c;
		door = d;
	}
	public Car4(String color, int door) {
		this.color = color;
		this.door = door;
	}
}
