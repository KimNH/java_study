package ch09;

public class InheritanceExam1 {
	public static void main(String[] args) {
		SportsCar sc = new SportsCar();
		SportsCar sc2 = new SportsCar();
		
		// 업캐스팅 Up Casting
		Car c = new SportsCar();
		c.move();
		
		Car c2 = sc;
		sc.openSunloof();
//		c2.openSunloof();
		
		
		Car c3 = (Car) sc2;
		
		// 다운캐스팅 Down Casting
		SportsCar sc3 = (SportsCar) c3;
		
		sc.move();
		sc.openSunloof();
		System.out.println(sc.getDoor());
		System.out.println(sc.getTire());
		System.out.println(sc.toString());
		System.out.println(sc2.toString());
	}
}
