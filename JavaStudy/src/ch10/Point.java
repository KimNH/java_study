package ch10;

import javax.xml.crypto.Data;

import exam.Exam10_2;

// java 파일명과 같은 클래스에만 public 사용
public class Point {
	int x;
	int y;
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	String getLocation() {
		return "x : " + x + ", y : " + y;
	}
}

class Point3D extends Point {
	int z;
	
	Point3D() {
		super(10, 10);
		Exam10_2 d = null;
//		Data d2 = new Data(10, "A");
		
//		d.setNum(num);	
	}
	
	@Override
	String getLocation() {
		return super.getLocation() + ", z : " + z;
//		return "x : " + x + ", y : " + y + ", z : " + z;
	}
}










