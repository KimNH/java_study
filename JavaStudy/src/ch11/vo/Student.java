package ch11.vo;

public class Student {
	private int grade;
	private int classNum;
	private String name;
	
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		if(grade > 4) {
			this.grade = 4;
		} else {	
			this.grade = grade;
		}
	}
	public int getClassNum() {
		return classNum;
	}
	public void setClassNum(int classNum) {
		this.classNum = classNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
