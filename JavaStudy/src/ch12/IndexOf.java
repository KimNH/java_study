package ch12;

public class IndexOf {
	public static void main(String args[]) {
		            //012345 678910     X
		String str = "Java 개발자 양성을 통한 취업연계과정!!";
		int idx = str.indexOf(" "); // a 개 
		System.out.println(idx);     // 4
		
		int idx2 = str.indexOf(" ", idx + 1); // 자 양
		System.out.println(idx2);             //  8
		
		int idx3 = str.indexOf(" ", idx2 + 1); // 을 통
		System.out.println(idx3);            //    12
		
		int idx4 = str.indexOf(" ", idx3 + 1);
		System.out.println(idx4);          
		
		int idx5 = str.indexOf(" ", idx4 + 1);
		System.out.println(idx5);          
		
		
	}
}
