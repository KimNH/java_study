package ch12;

import javax.swing.JFrame;

public class Replace {
	public static void main(String[] args) {
		JFrame j = new JFrame();
		j.setTitle("1111");
		j.setVisible(true);
		
		int age = 10;
		System.out.println(age + 10);
		
		String str1 = "자바 프로그래밍";
		String str2 = null;

		str2 = str1.replace("자바", "Java"); // str1의 값은 그대로 유지
		str2 = str2.replace(" ", "");
		System.out.println(str2);
		System.out.println(str1);
	}
}
