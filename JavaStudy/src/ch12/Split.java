package ch12;

public class Split {
	public static void main(String[] args) {
		// ?id=ggoreb&pw=1234&pageNum=1
		
		String str = "Java Secure Coding";
		String[] strs = str.split(" ");
		
		String temp = "";
		
		for (String s : strs) {
			temp += s + " ";
			System.out.println(temp);
		}
	}

}
