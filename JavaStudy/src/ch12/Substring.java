package ch12;

public class Substring {
	public static void main(String args[]) {
		String str = "Java 개발자 양성을 통한 취업연계과정!!";
		
		int idx1 = str.indexOf(" ");
		String s1 = str.substring(0, idx1);
		System.out.println(s1);
		
		int idx2 = str.indexOf(" ", idx1 + 1);
		String s2 = str.substring(idx1, idx2);
		System.out.println(s2);
		
		int idx3 = str.indexOf(" ", idx2 + 1);
		String s3 = str.substring(idx2, idx3);
		System.out.println(s3);
		
	}

}
