package ch12;

public class Test {
	public static void main(String[] args) {
		long start = 0L;
		long end = 0L;

		String s = "";
		start = System.currentTimeMillis();
		for (int i = 0; i < 50000; i++) {
//			s += i;
		}
		// unix time
		end = System.currentTimeMillis();
		System.out.println("String : " + (end - start));
		StringBuffer sb = new StringBuffer();
		start = System.currentTimeMillis();

		for (int i = 0; i < 5000000; i++) {
			sb.append(i);
		}
		end = System.currentTimeMillis();
		System.out.println("StringBuffer : " + (end - start));

		StringBuilder bb = new StringBuilder();
		start = System.currentTimeMillis();
		for (int i = 0; i < 5000000; i++) {
			bb.append(i);
		}
		end = System.currentTimeMillis();
		System.out.println("StringBuilder : " + (end - start));
	}

}
