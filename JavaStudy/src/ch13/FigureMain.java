package ch13;

public class FigureMain {
	public static void main(String[] args) {
		Figure f1 = new Tetragon();
		Figure f2 = new Triangle();
		
		f1.area(10, 10);
		f2.area(10, 10);
	}
}
