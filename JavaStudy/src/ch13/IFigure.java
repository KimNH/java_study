package ch13;

public interface IFigure {
	// 변수사용 불가
	
	// public static final int age = 100;
	int age = 100; // <== 상수
	
	
	// public abstract void area(int a, int b);
	void area(int a, int b);
}
