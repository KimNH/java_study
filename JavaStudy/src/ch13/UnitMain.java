package ch13;

public class UnitMain {
	public static void main(String[] args) {
		Tank t = new Tank("Black");
		Marine m = new Marine("Blue");
		
		move(t);
		move(m);
	}
	
	static void move(Unit u) { // <=== ������
		u.move("100, 100");
	}
}
