package ch14;

public class InnerClass {
	static int age;
	
	public class NormalClass {
		public void run() {
			System.out.println("Normal 실행");
		}
	}

	public static class StaticClass {
		public static void run1() {
			
		}
		public void run() {
			System.out.println("Static 실행" + age);
		}
	}

	public void method() {
		class LocalClass {
			void run() {
				System.out.println("Local 실행");
			}
		}
		new LocalClass().run();
	}
}








