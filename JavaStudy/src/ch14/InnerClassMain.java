package ch14;

public class InnerClassMain {
	static int age;
	
	public static void method() {
		System.out.println(age);
	}
	
	public static void main(String[] args) {
		InnerClass ic = new InnerClass();

		InnerClass.NormalClass nc = ic.new NormalClass();
		nc.run();

		InnerClass.StaticClass sc = new InnerClass.StaticClass();
		InnerClass.StaticClass.run1();
		sc.run();

		ic.method();
	}
}








