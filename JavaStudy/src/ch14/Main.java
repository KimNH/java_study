package ch14;

import java.io.IOException;

public class Main {
	public static void main(String[] args) {
		Button btn = new Button();
		MyClickListener mcl = new MyClickListener();
		btn.setOnClickListener(mcl);
		btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				System.out.println("버튼 클릭 - 인터넷 연결");
				Runtime runtime = Runtime.getRuntime();
		        try {
		            runtime.exec("C:/Program Files/Internet Explorer/iexplore.exe http://www.naver.com/");
		        } catch (IOException ex) {
		         
		        }  
			}
		});
	}
}
