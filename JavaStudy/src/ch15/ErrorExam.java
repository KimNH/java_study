package ch15;

import java.io.File;
import java.io.IOException;

public class ErrorExam {
	static void method1() throws RuntimeException {
		method2();
	}
	
	static void method2() throws RuntimeException {
//		try {
			System.out.println(4/0);
//		} catch(Exception e) {
//			
//		}
	}
	
	
	public static void main(String[] args) {
		File file = new File("dd");
		file.mkdir();
		
		try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		
		
		
		
		
		try {
			method1();
		} catch (Exception e) {
			System.out.println("0으로 나눌수 없습니다. 다시 입력해주세요.");
			e.printStackTrace();
		}
		
		int a = (int)1.2; // compile error

		// runtime error
		int b = 0;
		if(b != 0) {
			System.out.println(4 / b); // Arithmetic
		}
		
			System.out.println(new String("ddd").charAt(1)); // IndexOutOfBounds
			String str = null;
			System.out.println(str.equals("")); // NullPointer
		
			int[] arrs = new int[-1]; // NegativeArraySize
		
			String s = "Exception";
			int count = s.indexOf("a");
			int[] arrays = new int[count]; // NegativeArraySize
			System.out.println(arrays);
		
		System.out.println("프로그램 종료");
	}
}

	
	
	
	
	
	
	