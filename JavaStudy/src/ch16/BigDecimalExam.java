package ch16;

import java.math.BigDecimal;

public class BigDecimalExam {
	public static void main(String[] args) {
		float f1 = 1.6f;
		float f2 = 0.1f;
		System.out.println((float) (f1 * f2));

		double d1 = 1.6;
		double d2 = 0.1;
		System.out.println((double) (d1 * d2));

		BigDecimal e1 = new BigDecimal("1.6");
		BigDecimal e2 = new BigDecimal("0.1");
		System.out.println("������� : " + e1.multiply(e2));
	}
}








