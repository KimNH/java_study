package ch16;

import java.util.ArrayList;

public class Test {
	public static void main(String[] args) {
		int intMax = Integer.MAX_VALUE;
		String aa = "12345";
		int transNum = Integer.parseInt(aa);
		
		
		Object a = 1234; // Auto Boxing
		int b = (int) a; // Auto Un Boxing
		
		ArrayList<Integer> list = null;
		list.add(new Integer(10));
		list.add(10); // Auto Boxing
//		list.add("1-0");
		
//		Object a = new Integer(1234);
	}
}
