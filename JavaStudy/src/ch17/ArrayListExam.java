package ch17;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ArrayListExam {
	public static void main(String[] args) {
		List<Integer> lotto = new ArrayList<Integer>();
		Random ran = new Random();
		for(int i = 0; i < 6; i++) {
			int num = ran.nextInt(45) + 1;
			lotto.add(num);
		}
		System.out.println(lotto);
		
		List<String> list = new ArrayList<String>();
		list.add("1번");
		list.add("2번");
		list.add("=> 3번");
		list.add("=> 4번");
		System.out.println("ArrayList elements");
		for (int idx = 0; idx < list.size(); idx++) {
			System.out.println(list.get(idx));
		}
		list.remove(3);
		list.remove(2);
		list.add(0, "=> 3번");
		list.add(1, "=> 4번");
		System.out.println("ArrayList elements");
		for (int idx = 0; idx < list.size(); idx++) {
			System.out.println(list.get(idx));
		}
	}

}
