package ch17;

import java.util.Arrays;

public class MyList<E> {
	private int size = 0;
	private Object[] data = new Object[10];

	public void add(E e) {
		if (size >= data.length) {
			data = Arrays.copyOf(data, data.length + 10);
		}
		this.data[size] = e;
		size++;
	}

	E get(int idx) {
		return (E) this.data[idx];
	}

	@Override
	public String toString() {
		return "MyList [size=" + size + ", data=" + Arrays.toString(data) + "]";
	}
	
	

}









