package ch17;

import java.util.ArrayList;
import java.util.Random;

class MyClass {
	int age;
	String name;
}

public class MyListMain {
	public static void main(String[] args) {
		MyList<MyClass> list4 = new MyList<MyClass>();
		MyClass mc = new MyClass();
		mc.age = 10;
		list4.add(mc);
		MyClass mc2 = list4.get(0);
		System.out.println(mc2.age);
		

		MyList<Random> list3 = new MyList<Random>();
		list3.add(new Random());
		
		MyList<Integer> list2 = new MyList<Integer>();
		list2.add(123);

		
		ArrayList<String> al = new ArrayList<String>();
		al.add("1");
		al.add("2");
		al.add("3");
		al.add("4");
		System.out.println(al);
		
		MyList<String> list = new MyList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		list.add("7");
		list.add("8");
		list.add("9");
		list.add("10");
		System.out.println(list);
		list.add("11");
		System.out.println(list);
	}
}








