package ch18;

import java.util.List;
import java.util.Map;

public class DaoLauncher {
	
	@Override
	public String toString() {
		return "DaoLauncher []";
	}

	public static void main(String[] args) {
		try {
			Object obj = Class.forName("ch18.MemberDao").newInstance();
			MemberDao dao = (MemberDao) obj;
			System.out.println(dao.getMemberList());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
//		MemberDao dao = new MemberDao();
		
//		List<Map<String, String>> list2 = 
//				dao.getMemberList3();
//		for(int i = 0; i < list2.size(); i++) {
//			String id = list2.get(i).get("id");
//			System.out.println(list2.get(i));
//		}
//		
//		
//		
//		
//		
//		
//		
//		MemberDto dto2 = new MemberDto();
//		dao.addMember(dto2);
//		
//
//		List<MemberDto> list = dao.getMemberList();
//
//		for (int i = 0; i < list.size(); i++) {
//			MemberDto dto = list.get(i);
//
//			String id = dto.getId();
//			String pass = dto.getPass();
//			System.out.println(id + ", " + pass);
//		}
	}
}
