package ch18;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapExam {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("111", "aaa");
		map.put("112", "aab");
		map.put("113", "aac");
		map.put("114", "aad");
		map.put("115", "aaa");
		map.put("111", "zzz");
		

		Set<String> set = map.keySet();
		Iterator<String> iter = set.iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			String value = map.get(key);
			System.out.println(key + " : " + value);
		}
		for (Map.Entry<String, String> s : map.entrySet()) {
			String key = s.getKey();
			String value = s.getValue();
			System.out.println(key + " : " + value);
		}
	}
}
