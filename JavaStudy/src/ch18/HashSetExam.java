package ch18;

import java.util.HashSet;
import java.util.Set;

class Data {
	String name;
	
	Data() {}
	
	Data(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Data [name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}

public class HashSetExam {
	public static void main(String args[]) {
		HashSet<Data> hs = new HashSet<Data>();
		
		Data d1 = new Data();
		d1.name = "A";
		hs.add(d1);

		d1 = new Data();
		d1.name = "A";
		hs.add(d1);
		
		hs.add(new Data("B"));
		
		System.out.println(hs.toString());
	}
}











