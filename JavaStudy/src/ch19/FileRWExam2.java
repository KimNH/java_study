package ch19;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

public class FileRWExam2 {
	public static void main(String[] args) {
		FileWriter fw = null;
		OutputStream out = null;
		try {
			fw = new FileWriter("d:/text.txt");
			out = new FileOutputStream("d:/text2.txt");
			String text = "���� ���� ���Ƽ� �޿� ���� ������\n" + "�¼����� ��� �Ͼ� ������ ������ �޼ӿ�~";
			fw.write(text);
			
			out.write(text.getBytes());
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
