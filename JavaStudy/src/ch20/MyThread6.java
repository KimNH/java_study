package ch20;

public class MyThread6 extends Thread {
	private Counter counter;
	private int number;

	MyThread6(Counter counter, int number) {
		this.counter = counter;
		this.number = number;
	}

	public void run() {
		int i = 0;
		while (i < 1000) {
			counter.increment();
			try {
				int random = (int) Math.random();
				sleep(random);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
		System.out.println(number + " " + counter.getValue());
	}
}
