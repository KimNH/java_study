package ch20;

import ch19.FileDelete;

public class ThreadDelete {
	public static void main(String[] args) {
		FileDelete fd = new FileDelete();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					fd.deleteDirectory("d:/test");
					try {
						Thread.sleep(1000 * 10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}









