package ch20;

public class ThreadExam6 {
	public static void main(String[] args) {
		Counter c = new Counter();
		new MyThread6(c, 1).start();
		new MyThread6(c, 2).start();
		new MyThread6(c, 3).start();
		new MyThread6(c, 4).start();
	}
}
