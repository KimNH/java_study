package exam;

public interface Attackable {

	String attack();

}
