package exam;

public class AttackableProxy {
	private Attackable attackable;

	public AttackableProxy(Attackable attackable) {
		this.attackable 
		= attackable;
	}

	public void run() {
		System.out.println(attackable.attack());
	}
}
