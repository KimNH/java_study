package exam;

public class Automobile {
	void execute() {
		System.out.println("자동차");
	}

	class Tire {
		void execute() {
			System.out.println("타이어");
		}
	}

	static class Engine {
		void execute() {
			System.out.println("엔진");
		}
	}
}
