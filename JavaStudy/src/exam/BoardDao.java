package exam;

import java.util.ArrayList;
import java.util.List;

public class BoardDao {
	// 코드 작성
	public List<BoardDto> getBoardList() {
		// List 선언 및 초기화
		// 반복문 이용 BoardDto 생성 및 list add()
		List<BoardDto> list = new ArrayList<BoardDto>();
		for(int i = 0; i < 2; i++) {
			BoardDto dto = new BoardDto("제목-" + i, "내용-" + i);
			list.add(dto);
		}
		
		return list;
	}
}
