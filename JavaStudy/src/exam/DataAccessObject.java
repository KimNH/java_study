package exam;

public interface DataAccessObject {
	void insert();
	void select();
	void update();
	void delete();
}
