package exam;

public class Exam10_1 {
	public static void main(String[] args) {
		Exam10_1 e = new Exam10_1();
		e.print(new String[] { "A", "B", "C" });
		e.print(new Board(1, "1�� �Խù�"));
	}

	public void print(String[] str) {
		for(String s : str) { // for-each
			System.out.println(s);
		}
	}

	public void print(Board board) {
		System.out.println(board);
	}
}

class Board {
	private int num;
	private String title;

	Board(int num, String title) {
		this.num = num;
		this.title = title;
	}

	@Override
	public String toString() {
		return "num=" + num + ", title=" + title;
	}
}






