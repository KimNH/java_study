package exam;

public class Exam10_2 {
	int num;
	
	public void setNum(int num) {
		if(num > 100) {
			System.out.println("100 미만의 나이만 입력해주세요.");
		}
		this.num = num;
	}

	public static void main(String[] args) {
		Exam10_2 e = new Exam10_2();
		e.print(10);
		e.print("JAVA");
		e.print(new Data(10, "ABC"));
	}
	// [ 오버로딩 메소드 코드 작성 ]
	void print(int num) {
		System.out.println("입력된 정수는 " + num + " 입니다.");
	}
	void print(String str) {
		System.out.println("입력된 문자열은 " + str + " 입니다.");
	}
	void print(Data d) {
		System.out.println(d);
	}
}

class Data {
	int num;
	
	public void setNum(int num) {
		if(num > 100) {
			System.out.println("100 미만의 나이만 입력해주세요.");
		}
		this.num = num;
	}
	
	String str;
	
	public Data(int num, String str) {
		this.num = num;
		this.str = str;
	}
	
	@Override
	public String toString() {
		return "입력된 객체의 값은 숫자 : " + num + ", 문자 : " + str + "입니다.";
	}
	
}












