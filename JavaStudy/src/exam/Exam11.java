package exam;

public class Exam11 {
	public static void main(String[] args) {
		TV tv = new TV();
		// 코드 작성
		tv.setVolume(101);
		tv.setVolume(-1);
		tv.setVolume(50);
	}
}

class TV {
	private int volume;
	private int channel;

	public void setVolume(int volume) {
		if(volume > 100) {
			System.out.println("100 초과 불가");
			volume = 100;
		} else if(volume < 0) {
			System.out.println("0 미만 불가");
			volume = 0;
		}
		this.volume = volume;
		System.out.println("현재 소리 크기 : " + volume);
	}
	public void setChannel(int channel) {
		if(channel > 100) {
			System.out.println("100 초과 불가");
			channel = 100;
		} else if(channel < 0) {
			System.out.println("0 미만 불가");
			channel = 0;
		}
		this.channel = channel;
		System.out.println("현재 채널 : " + channel);
	}
	
	
	public int getVolume() {
		return volume;
	}
	public int getChannel() {
		return channel;
	}
	
}