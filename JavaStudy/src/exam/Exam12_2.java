package exam;

/**
 * @author  Lee Boynton
 * @author  Arthur van Hoff
 * @author  Martin Buchholz
 * @author  Ulf Zibis
 * @see     java.lang.Object#toString()
 * @see     java.lang.StringBuffer
 * @see     java.lang.StringBuilder
 * @see     java.nio.charset.Charset
 * @since   JDK1.0
  */
public class Exam12_2 {
	public static void main(String[] args) {
		String file = "/home/temp/Java.class";
		// indexOf, substring
		int sIdx = 0; // 시작위치
		int eIdx = 0; // 종료위치
		
		while(true) {
			eIdx = file.indexOf("/", sIdx + 1);
			
			if(eIdx == -1) {
				System.out.println(file.substring(sIdx));
				break;
			}
			
			String word = file.substring(sIdx, eIdx);
			System.out.println(word);
			sIdx = eIdx;
		}
		System.out.println("======================");
		
		// split
		String[] result1 = file.split("/");
		// [0] : ""
		// [1] : home [2] : temp
		for(int i = 0; i < result1.length; i++) {
//			if(!result1[i].equals("")) {
			if(result1[i].equals("") == false) {
				System.out.println("/" + result1[i]);
			}
		}
		
	}

}
