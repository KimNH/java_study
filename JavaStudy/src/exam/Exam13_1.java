package exam;

public class Exam13_1 {
	public static void main(String[] args) {
		Zerg zerg = new Zerg();
		AttackableProxy proxy1 = new AttackableProxy(zerg);
		proxy1.run();

		Zealot zealot = new Zealot();
		AttackableProxy proxy2 = new AttackableProxy(zealot);
		proxy2.run();
	}
}
