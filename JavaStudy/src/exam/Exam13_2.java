package exam;

public class Exam13_2 {
	public static void main(String[] args) {
		dbWork(new OracleDao());
		dbWork(new MySQLDao());
	}

	public static void dbWork(DataAccessObject dao) {
		dao.insert();
		dao.select();
		dao.update();
		dao.delete();
	}
}
