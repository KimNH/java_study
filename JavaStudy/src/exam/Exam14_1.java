package exam;

public class Exam14_1 {
	public static void main(String[] args) {
		Automobile a = new Automobile() {
			@Override
			void execute() {
				System.out.println("����");
			}
		};
		a.execute();
		
		Automobile a1 = 
			new Automobile();
		a1.execute();
		
		Automobile.Tire a2 =
			new Automobile().new Tire();
		a2.execute();
		
		Automobile.Engine a3 =
			new Automobile.Engine();
		a3.execute();
	}
}








