package exam;

public class Exam14_2 {
	static int age = 10;
	public static void main(String[] args) {
		Anonymous ano = new Anonymous();
		ano.field.run();
		ano.method1();
		
		ano.method1();
		
//		final int age = 10;
		
		ano.method2(new Vehicle() {
			@Override
			public void run() {
				int age2 = age + 10;
			}
		}); // Ctrl + 1
	}
}
