package exam;

import java.util.ArrayList;

public class Exam18_1 {
	public static void main(String[] args) {
		int num = 1000;
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(int i = 2; i < num; i++) {
			int checkNum = i;
			boolean isSosu = true;
			for(int j = 2; j < checkNum; j++) {
				// 소수여부 판단
				if(checkNum % j == 0) { // 약수가 존재하고 있음
					isSosu = false;
					break;
				}
			}
			
			if(isSosu == true) {
				// 소수 맞음
				list.add(checkNum);
			}
			
		}
		System.out.println("소수 개수 : " + list.size());
		
		
	}
}
