package exam;

import java.util.List;

public class Exam18_2 {
	public static void main(String[] args) {
		BoardDao dao = new BoardDao();
		List<BoardDto> list = dao.getBoardList();
		// 출력 코드 작성
		for(BoardDto dto : list) {
			System.out.println(
				dto.getTitle() + " / " + dto.getContent());
		}
	}

}
