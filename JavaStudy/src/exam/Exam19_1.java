package exam;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Exam19_1 {
	public static void main(String[] args) throws IOException {
		String path = "c:/java/document"; // 생성할 디렉토리의 경로
		// 디렉토리가 없는 경우 디렉토리 생성
		String str = "Eighty percent of success is showing up.\n" + "성공의 8할은 일단 출석하는 것이다.";
		// FileWriter를 사용하여 "c:/java/document/famous_saying.txt" 파일로 str 문자열 출력
		Writer writer = new FileWriter(path + "/famous_saying.txt");
		writer.write(str);
		writer.close();
	}
}










