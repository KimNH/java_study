package exam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Exam19_2 {
	public static void main(String[] args) throws IOException {
		String sourceFile = "c:/Windows/System32/drivers/etc/hosts";
		String copyPath = "c:/export"; // 복사 위치
		String copyFile = "hosts_copy"; // 복사 파일명
		// 복사될 위치에 디렉토리가 없다면 생성
		File dir = new File(copyPath);
		if(!dir.isDirectory()) {
			dir.mkdirs();
		}

		// 파일 복사
		FileInputStream in = new FileInputStream(sourceFile);
		FileOutputStream out = 
				new FileOutputStream(copyPath + "/" + copyFile);
		while(true) {
			int data = in.read();
			if(data == -1) break;
			out.write(data);
		}
		in.close();
		out.close();

	}
}









