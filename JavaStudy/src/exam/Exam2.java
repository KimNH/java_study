package exam;

public class Exam2 {
	public static void main(String[] args) {
		// int 형태의 score1 변수 선언
		int score1;
		// score1 변수에 90 대입
		score1 = 90;

		// int 형태의 score2 변수 선언
		int score2;
		// score2 변수에 80 대입
		score2 = 80;

		// System.out.println 을 사용하여 score1 + score2 의 값 출력
		// 출력 예) score1 + score2의 연산 결과는 xxx 입니다
		System.out.println(
			"score1 + score2의 연산 결과는 " + (score1 + score2) + "입니다.");
	}

}








