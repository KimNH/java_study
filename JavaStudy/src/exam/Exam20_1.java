package exam;

public class Exam20_1 {
	public static void main(String[] args) {
		MovieThread thread1 = new MovieThread();
		thread1.start();

		MusicRunnable music = new MusicRunnable();
		Thread thread2 = new Thread(music);
		thread2.start();
	}
}
