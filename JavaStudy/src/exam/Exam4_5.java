package exam;

public class Exam4_5 {
	public static void main(String[] args) {
		boolean isContinue = true;
		int count = 0;

		while (isContinue) {
			// 1. 주사위 던짐
			int num1 = (int) (Math.random() * 6 + 1);
			int num2 = (int) (Math.random() * 6 + 1);

//			count = count + 1; // 
			// 2. 던진 횟수 증가
//			++count;
			count += 1;
			
			// 코드 작성
			// 3. 결과 출력
			System.out.println("(" + num1 + ", " + num2 + ")");
			
			// 4. 종료 여부 판단
			if(num1 + num2 == 10) {
				isContinue = false;
			}
		}
		System.out.println("주사위를 던진 횟수 : " + count);
	}

}
