package exam;

public class Exam4_7 {
	public static void main(String[] args) {
		int num = 7;
		int center = num / 2 + 1;
		int leftSp = center - 1;
		int rightSp = center + 1;
		
		for(int i = 1; i <= num; i++) {
			for(int j = 1; j <= num; j++) {
				if(j > leftSp && j < rightSp) { // *
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			if(center > i) {
				leftSp--;
				rightSp++;
			} else {
				leftSp++;
				rightSp--;
			}
			System.out.println();
		}
		
	}
}





