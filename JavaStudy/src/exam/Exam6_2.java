package exam;

import java.util.Random;

public class Exam6_2 {
	static int age = 0; // 전역변수
	int score = 0;
	
	public static void main(String[] args) {
		age = 10;
		
		Random ran = new Random();
		System.out.println(ran.nextInt(5));
		
		System.out.println(new Random().nextInt(5));
		
		int a1 = 10;
		int a2 = 20;
		
		Exam6_2 e1 = new Exam6_2();
		Exam6_2 e2 = new Exam6_2();
		e1.age = 20;
		
		int[] arr = new int[4];
		System.out.println(arr.length);
		
		int n = 100;
		int total = sum(n);
		System.out.println("1 ~ " + n + " 까지의 합 : " + total);
	}

	// 메소드 작성
	public static int sum(int num) {
		int sum = 0;
		for (int i = 1; i <= num; i++) {
			sum += i;
		}
		return sum;
	}
}
