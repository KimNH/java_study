package exam;

import java.util.Arrays;

public class Exam6_4 {
	public static void main(String[] args) {
		char[] ch = {'z', 'y', 'd', 'a', 'b'};
		System.out.println("before");
		System.out.println(ch);
		Arrays.sort(ch);
		System.out.println("after");
		System.out.println(ch);
		
		
		
		int[] nums = { -1, -2, -3, -4, -5 };

		int max = getMaxValue(nums);
		System.out.println("max : " + max);

		int min = getMinValue(nums);
		System.out.println("min : " + min);
	}

	public static int getMaxValue(int[] nums) {
		int max = Integer.MIN_VALUE;
		// 코드 작성
		for (int i = 0; i < nums.length; i++) {
			if(nums[i] > max) {
				max = nums[i];
			}
		}
		return max;
	}

	public static int getMinValue(int[] nums) {
//		int min = 0;
		int min = Integer.MAX_VALUE;
		// 코드 작성
		for (int i = 0; i < nums.length; i++) {
//			if(nums[i] < min || i == 0 || min == 0) {
			if(nums[i] < min) {
				min = nums[i];
			}
		}
		return min;
	}

}



