package exam;

import java.util.Calendar;

public class ExamEx1 {
	
	public static void main(String[] args) {
		ExamEx1 cal = new ExamEx1();
		System.out.println("현재일자 : " + cal.getDate());
		System.out.println("현재시각 : " + cal.getTime());
	}

	Calendar cal = Calendar.getInstance();
	public String getDate() {
		int year = cal.get(Calendar.YEAR);
		
		int month = cal.get(Calendar.MONTH) + 1;
//		int month2 = cal.get(Calendar.MONTH + 1);
		
		int date = cal.get(Calendar.DAY_OF_MONTH);
		
		String m = "";
		if(month < 10) {
			m = "0" + month;
		} else {
			m = "" + month;
		}
		
		return "" + year + m + (date < 10 ? "0" + date : date);
	}

	public String getTime() {
//		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		return "" + (hour < 10 ? "0" + hour : hour) + 
			   (min < 10 ? "0" + min : min) + 
			   (sec < 10 ? "0" + sec : sec);
	}

}








