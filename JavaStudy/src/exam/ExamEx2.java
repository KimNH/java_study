package exam;

import java.util.Calendar;

public class ExamEx2 {

	public static void main(String[] args) {
		ExamEx2 cal = new ExamEx2();
		System.out.println(cal.getAddDate("20070327", 1000));
	}

	public String getAddDate(String sourceDate, int plusDate) {
		String year = sourceDate.substring(0, 4);
		String month = sourceDate.substring(4, 6);
		String date = sourceDate.substring(6); // 27
		
		int y = Integer.parseInt(year);
		int m = Integer.parseInt(month);
		int d = Integer.parseInt(date);
		
		Calendar cal = Calendar.getInstance();
		cal.set(y, m - 1, d);
		cal.add(Calendar.DATE, plusDate);
		
		int y2 = cal.get(Calendar.YEAR);
		int m2 = cal.get(Calendar.MONTH) + 1;
		int d2 = cal.get(Calendar.DATE);
		
		return "" + y2 + m2 + d2;
	}

}











