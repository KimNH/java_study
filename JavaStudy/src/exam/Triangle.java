package exam;

public class Triangle {
	float bottom;
	float height;
	String name;
	
	public Triangle() {
		
	}
	public Triangle(float bottom, float height) {
		this.bottom = bottom;
		this.height = height;
	}
	public Triangle(float bottom, float height, String name) {
		this.bottom = bottom;
		this.height = height;
		this.name = name;
	}



	// 넓이 구하기 메소드 (float)
	float area() {
		return bottom * height / 2;
	}
	
	// 밑변 저장 set 메소드 (void)
	void setBottom(float bottom) {
//		bottom = bo;
		this.bottom = bottom;
	}
	
	// 높이 저장 set 메소드 (void)
	void setHeight(float h) {
		height = h;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getBottom() {
		return bottom;
	}

	public float getHeight() {
		return height;
	}

}
