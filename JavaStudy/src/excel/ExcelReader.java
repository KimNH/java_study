package excel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ExcelReader {
	/**
	 * excel read
	 */
	public static void main(String[] args) {
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = null;

		File file = new File("data/exam.xls");
		System.out.println(file.getAbsolutePath());
		Workbook wb = null;
		try {
			wb = Workbook.getWorkbook(file); // book
			Sheet sheet = wb.getSheet(0); // sheet
			int rowCount = sheet.getRows();
			
			for(int row = 0; row < rowCount; row++) {
				map = new HashMap<String, String>();
				
				Cell seqNoC = sheet.getCell(0, row); // seqNo
				map.put("seqNo", seqNoC.getContents());
				
				Cell titleC = sheet.getCell(1, row); // title
				map.put("title", titleC.getContents());
				
				Cell contentC = sheet.getCell(2, row); // content
				map.put("content", contentC.getContents());
				
				list.add(map);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			wb.close();
		}
		
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).get("seqNo") + " / " + list.get(i).get("title") +
	          " / " + list.get(i).get("content"));
		}
	}
}









