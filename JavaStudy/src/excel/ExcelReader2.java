package excel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ExcelReader2 {
	/**
	 * excel read
	 */
	public static void main(String[] args) {
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> map = null;

		File file = new File("data/excel.xls");
		Workbook wb = null;
		try {
			wb = Workbook.getWorkbook(file); // book
			Sheet sheet = wb.getSheet(0); // sheet
			int rowCount = sheet.getRows();
			
			for(int row = 3; row < rowCount; row++) {
				map = new HashMap<String, String>();
				
				Cell seqNoC = sheet.getCell(1, row);
				map.put("seqNo", seqNoC.getContents());
				
				Cell num1 = sheet.getCell(13, row);
				map.put("num1", num1.getContents());
				
				Cell num2 = sheet.getCell(14, row);
				map.put("num2", num2.getContents());
				
				Cell num3 = sheet.getCell(15, row);
				map.put("num3", num3.getContents());
				
				Cell num4 = sheet.getCell(16, row);
				map.put("num4", num4.getContents());
				
				Cell num5 = sheet.getCell(17, row);
				map.put("num5", num5.getContents());
				
				Cell num6 = sheet.getCell(18, row);
				map.put("num6", num6.getContents());
				
				list.add(map);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			wb.close();
		}
		
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).get("seqNo") + " / " + 
					list.get(i).get("num1") + " / " + 
					list.get(i).get("num2") + " / " +
					list.get(i).get("num3") + " / " +
					list.get(i).get("num4") + " / " +
					list.get(i).get("num5") + " / " +
					list.get(i).get("num6"));
		}
	}
}









