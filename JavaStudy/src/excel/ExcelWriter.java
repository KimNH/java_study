package excel;

import java.io.File;
import java.io.IOException;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExcelWriter {
	/**
	 * excel write
	 */
	public static void main(String[] args) {

		File file2 = new File("data/excel_write.xls");
		WritableWorkbook wwb = null;
		WritableSheet ws = null;
		try {
			wwb = Workbook.createWorkbook(file2);
			wwb.createSheet("엑셀", 0);
			ws = wwb.getSheet(0);
			for (int i = 0; i < 5; i++) {
				ws.addCell(new Label(0, i, "제목" + i));
				ws.addCell(new Label(1, i, "내용" + i));
				ws.addCell(new Label(2, i, "작성자" + i));
				ws.addCell(new Label(3, i, "작성일자" + i));
			}
			wwb.write();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} finally {
			try {
				wwb.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
