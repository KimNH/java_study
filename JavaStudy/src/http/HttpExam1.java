package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpExam1 {
	public static String get(String url) throws ClientProtocolException, IOException {
		// 응답결과 문자열 저장
		StringBuffer data = new StringBuffer();

		// 아파치에서 제공해주는 라이브러리 HttpClient 사용
		HttpGet http = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		response = client.execute(http);

		// response로 받은 응답결과를 문자열로 변환
		BufferedReader br = null;
		br = new BufferedReader(
				new InputStreamReader(
						response.getEntity().getContent(), "utf-8"));
		
		String line = null;
		
		while ((line = br.readLine()) != null) {
			data.append(line + "\n");
		}
		
		return data.toString();
	}
	
	public static void main(String[] args) {
		String url = "http://nlotto.co.kr/gameResult.do?method=byWin&drwNo=751";
		String result;
		try {
			result = get(url);
			int pIdx = result.indexOf("<p class=\"number\">");
			int pIdx2 = result.indexOf("</p>", pIdx + 1);
			
			String p = result.substring(pIdx, pIdx2);
			System.out.println(p);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}













