package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class HttpExam2 {
	public static String get(String url) throws ClientProtocolException, IOException {
		// 응답결과 문자열 저장
		StringBuffer data = new StringBuffer();

		// 아파치에서 제공해주는 라이브러리 HttpClient 사용
		HttpGet http = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		response = client.execute(http);

		// response로 받은 응답결과를 문자열로 변환
		BufferedReader br = null;
		br = new BufferedReader(
				new InputStreamReader(
						response.getEntity().getContent(), "utf-8"));
		
		String line = null;
		
		while ((line = br.readLine()) != null) {
			data.append(line + "\n");
		}
		
		return data.toString();
	}
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		String url = "http://ggoreb.com/data1.jsp";
		String result = get(url);
		// JSON - Java Script Object Notation
		// {} - 객체   or     [] - 리스트
		// "Key" : "Value"
		System.out.println(result);
		
		ObjectMapper om = new ObjectMapper();
		Map<String, Object> map = 
			om.readValue(
				result, new TypeReference<Map<String, Object>>() {});
		int age = (int) map.get("age");
		String name = (String) map.get("name");
		System.out.println(age + "//" + name);
	}

}













