package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class HttpExam3 {
	public static String get(String url) throws ClientProtocolException, IOException {
		// 응답결과 문자열 저장
		StringBuffer data = new StringBuffer();

		// 아파치에서 제공해주는 라이브러리 HttpClient 사용
		HttpGet http = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		response = client.execute(http);

		// response로 받은 응답결과를 문자열로 변환
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

		String line = null;

		while ((line = br.readLine()) != null) {
			data.append(line + "\n");
		}

		return data.toString();
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		String url = "http://ggoreb.com/data2.jsp";
		String result = get(url);

		ObjectMapper om = new ObjectMapper();
		List<Map<String, Object>> list = om.readValue(result, new TypeReference<List<Map<String, Object>>>() {
		});
		
		Map<String, Object> map = list.get(0);
		
		int age = (int) map.get("age");
		
		System.out.println(map.get("age"));
		System.out.println(map.get("name"));
		
		// Generic - 데이터의 안정성을 위해서 사용
		HashMap<Integer, String> m = new HashMap<Integer, String>();
		m.put(1, "A");
		m.put(2, "B");
		m.put(2, "BBBB");
		m.put(3, "A");
//		m.put("A", 100);
		
		m.get(1);
		
//		List l2 = new LinkedList();
		List<Integer> l = new ArrayList<Integer>();
		l.add(1);
		l.get(0);
		
		List<Map<String, Integer>> lm = 
			new ArrayList<Map<String, Integer>>();
		lm.get(0);
			 
		
//		l.add("aaa");
		
		
		
	}









}
