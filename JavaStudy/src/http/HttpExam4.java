package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class HttpExam4 {
	public static String get(String url) throws ClientProtocolException, IOException {
		// 응답결과 문자열 저장
		StringBuffer data = new StringBuffer();

		// 아파치에서 제공해주는 라이브러리 HttpClient 사용
		HttpGet http = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		response = client.execute(http);

		// response로 받은 응답결과를 문자열로 변환
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

		String line = null;

		while ((line = br.readLine()) != null) {
			data.append(line + "\n");
		}

		return data.toString();
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		String url = "http://ggoreb.com/data3.jsp";
		String result = get(url);

		ObjectMapper om = new ObjectMapper();
		List<Map<String, Object>> list = om.readValue(result, new TypeReference<List<Map<String, Object>>>() {
		});
		
		for(int i = 0; i < list.size(); i++) {
			Map<String, Object> map = list.get(i);
			List<String> address = (List<String>) map.get("address");
			int age = (int) map.get("age");
			String name = (String) map.get("name");
			
			System.out.println(address);
			System.out.println(age);
			System.out.println(name);
		}
		
		
		
		
	}









}
