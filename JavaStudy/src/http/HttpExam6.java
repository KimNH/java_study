package http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class HttpExam6 {
	public static String get(String url) throws ClientProtocolException, IOException {
		// 응답결과 문자열 저장
		StringBuffer data = new StringBuffer();

		// 아파치에서 제공해주는 라이브러리 HttpClient 사용
		HttpGet http = new HttpGet(url);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;
		response = client.execute(http);

		// response로 받은 응답결과를 문자열로 변환
		BufferedReader br = null;
		br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

		String line = null;

		while ((line = br.readLine()) != null) {
			data.append(line + "\n");
		}

		return data.toString();
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyChQ_zS2piXcawpDSI-O16hPC4bPhSJ94U&q=%EC%9B%90%ED%94%BC%EC%8A%A4&maxResults=20";
		String result = get(url);

		// JSON Parsing
		ObjectMapper om = new ObjectMapper();
		Map<String, Object> map = 
			om.readValue(
				result, new TypeReference<Map<String, Object>>() {});
		
		List<Map<String, Object>> items = (List<Map<String, Object>>) map.get("items");
		
		for (int i = 0; i < items.size(); i++) {
			Map<String, Object> item = items.get(i);
			Map<String, Object> id = (Map<String, Object>) item.get("id");
			String videoId = (String) id.get("videoId");
			
			Map<String, Object> snippet = (Map<String, Object>) item.get("snippet");
			String title = (String) snippet.get("title");
			
			Map<String, Object> thumbnails = (Map<String, Object>) snippet.get("thumbnails");
			Map<String, Object> def = (Map<String, Object>) thumbnails.get("default");
			String url2 = (String) def.get("url");
			
			System.out.println(videoId + " " + title + " " + url2);
		}
	}




}




