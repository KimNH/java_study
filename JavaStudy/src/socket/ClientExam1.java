package socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientExam1 {
	public static void main(String[] args) {
		Socket socket = null;

		try {
			socket = new Socket("192.168.0.190", 20000);
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			writer.println("�ȳ�");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
