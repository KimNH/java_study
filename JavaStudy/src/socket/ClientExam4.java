package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

class ClientThread extends Thread {
	BufferedReader reader;
	
	ClientThread(BufferedReader reader) {
		this.reader = reader;
	}
	
	@Override
	public void run() {
		while(true) {
			String msg;
			try {
				msg = reader.readLine();
				System.out.println(msg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
public class ClientExam4 {
	public static void main(String[] args) {
		Socket socket = null;

		try {
			socket = new Socket("192.168.0.190", 20000);
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			
			InputStream in = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader reader = new BufferedReader(isr);
			
			ClientThread ct = new ClientThread(reader);
			ct.start();
			
			Scanner scan = new Scanner(System.in);
			while(true) {
				String msg = scan.nextLine();
				writer.println(msg);
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
