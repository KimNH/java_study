package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerExam3 {
	public static void main(String[] args) {
		ServerSocket sSocket = null; // 서버 사용 소켓
		Socket socket = null; // 클라이언트와 연결될 소켓

		try {
			sSocket = new ServerSocket(20000);
			System.out.println("클라이언트 접속 대기중");
			socket = sSocket.accept();

			InputStream in = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(in, "ms949");
			BufferedReader reader = new BufferedReader(isr);
			
			OutputStream out = socket.getOutputStream();
			PrintWriter writer = new PrintWriter(out, true);

			// 클라이언트로 부터 받은 메세지
			while(true) {
				String msg = reader.readLine();
				System.out.println(msg);
				writer.println(msg);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
			if (sSocket != null) {
				try {
					sSocket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
