package socket.multi.client;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * 서버 메세지 수신
 */
public class MessageReceiver extends Thread {
	private BufferedReader reader;

	public MessageReceiver(BufferedReader reader) {
		this.reader = reader;
	}
	
	@Override
	public void run() {
		boolean isContinue = true;
		while(isContinue) {
			try {
				String message = reader.readLine();
				if(message == null) {
					isContinue = false;
				}
				System.out.println(message);
			} catch (IOException e) {
				isContinue = false;
			}
		}
	}
}






